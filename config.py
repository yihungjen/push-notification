import sys

class __config():
    def __init__(self):
        self.__load()

    def __load(self):
        import json
        import os
        import requests
        config = {}
        with open(os.environ['HOME'] + '/.push_bullet.json') as p:
            config = json.load(p)

        self.auth = requests.auth.HTTPBasicAuth(
            config['user'],
            config['pass']
        )

        devs = requests.get(
            'https://api.pushbullet.com/v2/devices',
            auth=self.auth
        ).json()

        self.devices= {}
        for dev in devs['devices']:
            if not dev['active'] or not dev['pushable']:
                continue
            self.devices[dev['nickname']] = {
                unicode(k): dev[k] for k in ('iden', 'type')
            }

    def __getattr__(self, name):
        pass

sys.modules[__name__] = __config()
