#!/usr/bin/env python

def push(auth=None, target=None, ptype=None, pdata={}):
    import requests
    '''
    Method to push message with payload
    '''
    # capture the payload data
    # NOTE: we do not check for type
    payload = pdata

    # Attach type of message
    payload['type'] = ptype

    # Attach target device if specified
    if target is not None:
        payload['device_iden'] = target

    resp = requests.post(
        'https://api.pushbullet.com/v2/pushes',
        auth=auth,
        data=payload
    )

    return resp.json()
